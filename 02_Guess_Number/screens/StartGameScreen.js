import React, { useState, useEffect } from 'react';
import {
	Text,
	View,
	StyleSheet,
	Button,
	TouchableWithoutFeedback,
	Keyboard,
	Alert,
	Dimensions,
	ScrollView,
	KeyboardAvoidingView
} from 'react-native';

import Colors from "../constants/colors";
import Card from "../components/Card";
import Input from "../components/Input";
import NumberContainer from "../components/NumberContainer";
import TitleText from "../components/TitleText";
import MainButton from "../components/MainButton";

const StartGameScreen = props => {
	
	const [ enteredValue, setEnteredValue ] = useState('');
	const [ confirmed, setConfirmed ] = useState(false);
	const [ selectedNumber, setSelectedNumber ] = useState(0);
	const [ buttonWidth, setButtonWidth ] = useState(Dimensions.get('window').width / 4)
	
	const numberInputHandler = inputText => {
		setEnteredValue(inputText.replace(/[^0-9]/g, ''));
	};
	
	const resetInputHandler = () => {
		setEnteredValue('');
		setConfirmed(false);
	};
	
	useEffect(() => {
		const updateLayout = () => {
			setButtonWidth(Dimensions.get('window').width / 4);
		}
		
		Dimensions.addEventListener('change', updateLayout);
		
		return () => {
			Dimensions.removeEventListener('change', updateLayout);
		}
	})
	
	const confirmInputHandler = () => {
		const chosenNumber = parseInt(enteredValue);
		if ( isNaN(chosenNumber) || chosenNumber <= 0 || chosenNumber > 99 ) {
			Alert.alert(
				'Invalid NumberContainer',
				'NumberContainer has to a number between 1 and 99',
				[ {
					text: 'OK',
					style: 'destructive',
					onPress: resetInputHandler
				} ]);
			return;
		}
		setConfirmed(true);
		setSelectedNumber(chosenNumber);
		setEnteredValue('');
		Keyboard.dismiss();
	};
	
	let confirmedOutput;
	
	if ( confirmed ) {
		confirmedOutput = (
			<Card style={ styles.summaryContainer }>
				<Text>You selected</Text>
				<NumberContainer>{ selectedNumber }</NumberContainer>
				<MainButton onPress={ () => props.onStartGame(selectedNumber) }>
					START GAME
				</MainButton>
			</Card>
		)
	}
	
	return (
		<ScrollView>
			<KeyboardAvoidingView behavior={ 'position' } keyboardVerticalOffset={ 30 } >
				<TouchableWithoutFeedback
					onPress={ () => {
						Keyboard.dismiss();
					} }
				>
					<View style={ styles.root }>
						<TitleText style={ styles.title }>Start a New Game!</TitleText>
						<Card style={ styles.inputContainer }>
							<Text>Select a Number</Text>
							<Input
								style={ styles.input }
								blurOnSubmit
								autoCapitalize={ 'none' }
								autoCorrect={ false }
								keyboardType={ 'number-pad' }
								maxLength={ 2 }
								onChangeText={ numberInputHandler }
								value={ enteredValue }
							/>
							<View style={ styles.buttonsContainer }>
								<View style={ { width: buttonWidth } }>
									<Button
										title={ 'Reset' }
										onPress={ resetInputHandler }
										color={ Colors.secondary }
									/>
								</View>
								<View style={ { width: buttonWidth } }>
									<Button
										title={ 'Confirm' }
										onPress={ confirmInputHandler }
										color={ Colors.primary }
									/>
								</View>
							</View>
						</Card>
						{ confirmedOutput }
					</View>
				</TouchableWithoutFeedback>
			</KeyboardAvoidingView>
		</ScrollView>
	);
};

const styles = StyleSheet.create({
	root: {
		flex: 1,
		padding: 10,
		alignItems: 'center'
	},
	title: {
		fontSize: 18,
		marginVertical: 10
	},
	inputContainer: {
		width: '80%',
		minWidth: 300,
		maxWidth: '95%',
		alignItems: 'center'
	},
	buttonsContainer: {
		flexDirection: 'row',
		width: '100%',
		justifyContent: 'space-around',
		paddingHorizontal: 15
	},
	input: {
		width: 50,
		textAlign: 'center'
	},
	summaryContainer: {
		marginTop: 20,
		alignItems: 'center'
	}
});

export default StartGameScreen;
