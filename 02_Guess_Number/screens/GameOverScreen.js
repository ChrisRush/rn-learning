import React from 'react';
import { Text, View, StyleSheet, Button, Image, Dimensions, ScrollView } from 'react-native';

import DefaultStyles from "../constants/default-styles";
import Colors from "../constants/colors";

import TitleText from "../components/TitleText";
import MainButton from "../components/MainButton";

const GameOverScreen = props => {
	return (
		<ScrollView>
			<View style={ styles.root }>
				<TitleText>The Game is Over!</TitleText>
				<View style={ styles.imageContainer }>
					<Image
						source={ require('../assets/success.png') }
						style={ styles.image }
						resizeMode={ 'cover' }
					/>
				</View>
				<View style={ styles.resultContainer }>
					<Text style={ { ...DefaultStyles.bodyText, ...styles.resultText } }>
						Your phone needed <Text style={ styles.highlight }>{ props.roundsNumber }</Text> rounds
						to guess number <Text style={ styles.highlight }>{ props.userNumber }</Text>
					</Text>
				</View>
				<View style={ styles.buttonContainer }>
					<MainButton onPress={ props.onRestart }>NEW GAME</MainButton>
				</View>
			</View>
		</ScrollView>
	);
};

const styles = StyleSheet.create({
	root: {
		flex: 1,
		alignItems: 'center',
		justifyContent: 'center',
		marginTop: 10,
		paddingVertical: 10
	},
	buttonContainer: {
		padding: 5,
	},
	imageContainer: {
		width: Dimensions.get('window').width * 0.7,
		height: Dimensions.get('window').width * 0.7,
		borderRadius: Dimensions.get('window').width * 0.7 / 2,
		borderWidth: 3,
		borderColor: 'black',
		overflow: 'hidden',
		marginVertical: Dimensions.get('window').height / 30
	},
	image: {
		width: '100%',
		height: '100%'
	},
	resultContainer: {
		marginHorizontal: 50,
		marginVertical: Dimensions.get('window').height / 60
	},
	resultText: {
		textAlign: 'center',
		fontSize: Dimensions.get('window').height < 600 ? 14 : 18
	},
	highlight: {
		color: Colors.primary,
		fontFamily: 'open-sans-bold'
	}
});

export default GameOverScreen;
