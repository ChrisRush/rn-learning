import React, { useState } from "react";
import { Button, TextInput, View, StyleSheet, Modal } from "react-native";

const ShoppingInput = props => {
	
	const [ enteredShoppingItem, setEnteredShoppingItem ] = useState('');
	
	const goalInputHandler = (enteredText) => {
		setEnteredShoppingItem(enteredText);
	}
	
	const addShoppingItemHandler = () => {
		props.onAddShoppingItem(enteredShoppingItem);
		setEnteredShoppingItem('');
	}
	
	return (
		<Modal visible={ props.visible } animationType={ 'slide' }>
			<View style={ styles.inputContainer }>
				<TextInput
					placeholder="Course Goal"
					style={ styles.input }
					onChangeText={ goalInputHandler }
					value={ enteredShoppingItem }
				/>
				<View style={ styles.buttonContainer }>
					<View style={ styles.button }>
						<Button
							style={ styles.button }
							title={ 'CANCEL' }
							color={ 'red' }
							onPress={ props.onCancel }
						/>
					</View>
					<View style={ styles.button }>
						<Button
							style={ styles.button }
							title="ADD"
							onPress={ addShoppingItemHandler }
						/>
					</View>
				</View>
			</View>
		</Modal>
	)
}

const styles = StyleSheet.create({
	inputContainer: {
		flex: 1,
		justifyContent: 'center',
		alignItems: 'center'
	},
	input: {
		width: '80%',
		borderColor: 'black',
		borderWidth: 1,
		padding: 10,
		marginBottom: 20
	},
	buttonContainer: {
		flexDirection: 'row',
		justifyContent: 'space-around',
		width: '60%'
	},
	button: {
		width: '40%'
	}
})

export default ShoppingInput;