import React, { useState } from "react";
import { Button, StyleSheet, Text, View, TextInput, ScrollView, FlatList } from 'react-native';
import ShoppingItem from "./components/ShoppingItem";
import ShoppingInput from "./components/ShoppingInput";

export default function App() {
	const [ shoppingList, setShoppingList ] = useState([]);
	const [ isAddMode, setIsAddMode ] = useState(false);
	
	const addItemHandler = (shoppingItemName) => {
		setShoppingList(currentItems => [ ...currentItems, { id: Math.random().toString(), value: shoppingItemName } ]);
		setIsAddMode(false);
	}
	
	const removeItemHandler = (id) => {
		setShoppingList(currentItems => {
			return currentItems.filter((item) => item.id !== id);
		})
	}
	
	const cancelShoppingItemAdditionHandler = () => {
		setIsAddMode(false);
	}
	
	return (
		<View style={ styles.screen }>
			<Button title={ 'Add new item' } onPress={ () => setIsAddMode(true) }/>
			<ShoppingInput visible={ isAddMode } onAddShoppingItem={ addItemHandler } onCancel={ cancelShoppingItemAdditionHandler }/>
			<FlatList
				data={ shoppingList }
				renderItem={
					itemData => (
						<ShoppingItem
							id={ itemData.item.id }
							title={ itemData.item.value }
							onDelete={ removeItemHandler }
						/>
					) }
			/>
		</View>
	);
}

const styles = StyleSheet.create({
	screen: {
		padding: 50
	}
});
